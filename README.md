# vue-template-jack

The starting point I use for my Vue projects.

This is a [Vue CLI](https://cli.vuejs.org/) scaffolded project with [Vuex](https://vuex.vuejs.org/),
[Vue Router](https://router.vuejs.org/), and [TypeScript](https://www.typescriptlang.org/) configured and enabled.

[Tailwind CSS](https://tailwindcss.com/) is installed and set up with PurgeCSS for production.

[ESLint](https://eslint.org/) and [Prettier](https://prettier.io/) are installed and configured to be interoperable via `eslint-plugin-prettier`.

## Project setup

```
yarn
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```
